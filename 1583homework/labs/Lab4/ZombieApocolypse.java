import java.util.Scanner;

public class ZombieApocolypse
{
    public static void main(String[] args)
    {
        //setup game data
        int columns = 10;
        int rows = 10;
        int playerX = 0;
        int playerY = 0;
        int ZombieX = 5;
        int ZombieY = 5;
        int MallX = 9;
        int MallY = 9;
        boolean Gameover = false;
        Scanner input = new Scanner(System.in);
        //game loop
        while (Gameover == false)
        {
            //draw game screen
            System.out.println("\033[H\033[2J");
            for(int i = 0; i < rows; i++) 
                {  
                for (int j = 0; j < columns; j++) 
                    
                        if (i == playerY && j == playerX) 
                        {
                            System.out.print("P ");
                        } 
                        else if(j == ZombieX && i == ZombieY) 
                        {
                            System.out.print("Z ");
                        } 
                        else if(i == MallX && j == MallY)
                        {
                        System.out.print("W ");
                        }
                        else
                        {
                            System.out.print("- ");
                        }
                    System.out.print("\n");
                }    
            // Get player Input
            String playerMove = input.nextLine();
            
            //execute player action
            if (playerMove.equals("w"))
            {
                playerY--;
            }
            else if (playerMove.equals("s"))
            {
                playerY++;
            }
            else if (playerMove.equals("d"))
            {
                playerX++;
            }
            else if (playerMove.equals("a"))
            {
                playerX--;
            }
            //execute monster action
            boolean hasMoved = false;
            
            while (!hasMoved) 
            {
                
            
                int Zombiechoice = (int) (Math.random()*4);
            
                if (Zombiechoice == 0) 
                {
                    if (ZombieX < rows-1) 
                    {
                        ZombieX++;
                        hasMoved = true;
                    }
                    
                } 
                else if (Zombiechoice == 1) 
                {
                    if (ZombieX > 0) 
                    {
                        ZombieX--;
                        hasMoved = true;
                    }
                    
                } 
                else if (Zombiechoice == 2) 
                {
                    if (ZombieX > 0) 
                    {
                        ZombieY--;
                        hasMoved = true;
                        
                    }
            
                } 
                else if (Zombiechoice == 3) 
                {
                    if (ZombieY < columns-1) 
                    {
                        ZombieY++;
                        hasMoved = true;
                    }
                }
            //check lose condition
            if (playerX == ZombieX && playerY == ZombieY) 
            {
                Gameover = true;
                System.out.println("You got eaten");
            }
            //check win condition
            if (playerX == MallX && playerY == MallY) 
            {
                Gameover = true;
                System.out.println("You Survived");
            }
          }
        }   
    }
}