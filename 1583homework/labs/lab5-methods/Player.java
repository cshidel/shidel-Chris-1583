public class Player
{
        //game data --> accessible by all game mathods, scoped for as long as Game class runs.
        private static String image;
        private static int width;
        private static int height;
        private static double x;
        private static double y;
        private static boolean isAttacking;

        
        
        public static void draw()
        {
                //draw scene
                StdDraw.picture(x+width/2, y+height/2, image);
        }
        
        public static void start()
        {
                //setup scene image
                image = "assets/aimer.png";
                width = 32;
                height = 32;
                x = Scene.getWidth()/2;
                y = Scene.getHeight()/2;
                isAttacking = false;
        }
        
        public static void move()
        {
            x = StdDraw.mouseX() - width/2;
            y = StdDraw.mouseY() - height/2;
        }
        
        public static double getX()
        {
            return x;
        }
        
        public static double getY()
        {
            return y;
        }
        
        public static double getHeight()
        {
            return height;
        }
        
        public static double getWidth()
        {
            return width;
        }
        
        public static boolean isAttacking()
        {
            return isAttacking;
        }
        
        public static void attack()
        {
            if (StdDraw.mousePressed())
                    isAttacking = true;
            else
                    isAttacking = false;
        }
}


