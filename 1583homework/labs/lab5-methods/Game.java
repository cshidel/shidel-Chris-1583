public class Game
{
    private static boolean gameOver = false;
    private static int score = 0;
    
    public static void main(String[] args)
    {
        start();                   //start game
        while (gameOver == false)  //game loop:
        {
            update();              //1. update game
            render();              //2. render game
        }
    }
    
    public static void render()
    {
        Scene.draw();             //draw scene
        Enemy.draw();             //draw enemy
        Player.draw();            //draw player
        StdDraw.text(64, 32, "Score:" + score);     //draw score
        StdDraw.show(100);        //show graphics
    }
    
    public static void update()
    {
        //check for input
        Player.move();
        Player.attack();
        //update player
        long now = System.currentTimeMillis();      //update enemy data
        if (now - Enemy.getAge() > 1000)
        {
            Enemy.move();
        }
        if (Enemy.isTouching() && Player.isAttacking())
        {
            Enemy.move();
            score++;
        }
    }
    
    public static void start()
    {
        Scene.start();   //setup scene data
        Enemy.start();   //setup enemy data
        Player.start();  //setup player data
    }
}