public class Enemy
{
    //enemy data --> accessible by all game methods, scoped for as long as Game class runs.
    private static String image;
    private static int width;
    private static int height;
    private static double x;
    private static double y;
    
    //draw enemy
    public static void draw()
    {
        StdDraw.picture(x+width/2, y+height/2, image);
    }
    
    public static void start()
    {
        //setup scene image
        image = "assets/target.png";
        width = 32;
        height = 32;
        x = Math.random() * Scene.getWidth() - width;
        y = Math.random() * Scene.getHeight() - height;
        age = System.currentTimeMillis();
    }
    
    public static void move()
    {
        x = Math.random() * Scene.getWidth() - width;
        y = Math.random() * Scene.getHeight() - height;
        age = System.currentTimeMillis();
    }
    
    private static long age;
    
    public static long getAge()
    {
        return age;
    }
    
    public static double getX()
    {
        return x;
    }
        
    public static double getY()
    {
        return y;
    }
        
    public static double getHeight()
    {
        return height;
    }
        
    public static double getWidth()
    {
        return width;
    }
    
    public static boolean isTouching()
    {
        return ( Player.getX() <= (Enemy.getX() + Enemy.getWidth())
                            && Enemy.getX() <= (Player.getX() + Player.getWidth())
                            && Player.getY() <= (Enemy.getY() + Enemy.getHeight())
                            && Enemy.getY() <= (Player.getY() + Player.getHeight())
                );
    }
}